# This official base image contains node.js and npm
FROM node:12

# MAINTAINER Beatriz Souza <beatrizasouza13@gmail.com>
WORKDIR /opt
COPY package*.json ./

# Download the required packages
RUN npm install

COPY . .

EXPOSE 3000

# Make the application run when running the container
ENTRYPOINT [ "npm", "run" ]
CMD ["test" ]