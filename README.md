# Rick and Morty Api Tests

Contract tests for rick and morty Api

## Using:
* JavaScript
* CodeceptJS
* Axios
* JsonSchema

## How to run

### Installing dependencies:
```
$ npm install
```

### Running tests:
```
$ npm run test
```

