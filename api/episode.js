const axios = require('axios')

async function getEpisode() {
  return await axios.get('https://rickandmortyapi.com/api/episode/' ,
  )
}

exports.getEpisode = getEpisode
