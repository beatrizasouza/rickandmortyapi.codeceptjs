exports.config = {
  tests: './tests/*_test.js',
  output: './output',
  helpers: {
    AXIOS: {
       require: "codeceptjs-axios"
     }
  },
  name: 'rickandmortyapi.codeceptjs'
}
