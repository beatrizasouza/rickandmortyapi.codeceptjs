const schema =
{
  "definitions": {},
  "type": "object",
  "required": [
    "info",
    "results"
  ],
  "properties": {
    "info": {
      "type": "object",
      "required": [
        "count",
        "pages",
        "next",
        "prev"
      ],
      "properties": {
        "count": {
          "type": "integer",
        },
        "pages": {
          "type": "integer",
        },
        "next": {
          "type": "string",
        },
        "prev": {
          "type": "string"
        }
      },
      "results": {
        "type": "array",
        "items": {
          "type": "object",
          "required": [
            "id",
            "name",
            "air_date",
            "episode",
            "characters",
            "url",
            "created"
          ],
          "properties": {
            "id": {
              "type": "integer",
            },
            "name": {
              "type": "string",
            },
            "air_date": {
              "type": "string",
            },
            "episode": {
              "type": "string",
              "examples": [
                "S01E01"
              ],
            },
            "characters": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "url": {
                "type": "string"
              },
              "created": {
                "type": "string",
                "examples": [
                  "2017-11-10T12:56:33.798Z"
                ],
              }
            }
          }
        }
      }
    }
  }
}

exports.episodeSchema = schema
