const chai = require("chai");
const expect = chai.expect;
const { getEpisode } = require('../api/episode')
const {
  episodeSchema
} = require('../schemas/episode_schema')

const Validator = require('jsonschema').Validator
const validadeSchema = new Validator();

Feature("Validade Get Episode");

BeforeSuite(async () => {
  episode = await getEpisode()
})

Scenario('Validate status code', async () => {
  expect(episode.status).to.eq(200)
})

Scenario('Validate Episode Schema', async () => {
  const result = validadeSchema.validate(episode.data, episodeSchema)
  expect(result.errors).to.length(0)
})
